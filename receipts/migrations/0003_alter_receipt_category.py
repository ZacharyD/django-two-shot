# Generated by Django 4.1.3 on 2022-12-03 05:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ("receipts", "0002_receipt_account"),
    ]

    operations = [
        migrations.AlterField(
            model_name="receipt",
            name="category",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="receipts",
                to="receipts.expensecategory",
            ),
        ),
    ]
